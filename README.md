# studentManager

#### 介绍
一个用Gin框架写的分布式学生信息管理系统

##### 数据需求

系统中各表结构

①hubu_student表

| 字段       | 含义 | 数据类型 | 长度 | 是否允许为空 | 备注 |
| ---------- | ---- | -------- | ---- | ------------ | ---- |
| SID        | 学号 | varChar  | 255  | 否           | 主键 |
| Name       | 姓名 | varChar  | 255  | 否           |      |
| Sex        | 性别 | varChar  | 255  | 否           | M或F |
| Class      | 班级 | varChar  | 255  | 否           |      |
| Major      | 专业 | varChar  | 255  | 否           |      |
| Department | 学院 | varChar  | 255  | 否           |      |

②hubu_teacher表

| 字段 | 含义 | 数据类型 | 长度 | 是否允许为空 | 备注 |
| ---- | ---- | -------- | ---- | ------------ | ---- |
| TID  | 工号 | varChar  | 255  | 否           | 主键 |
| Name | 姓名 | varChar  | 255  | 否           |      |

③hubu_course表

| 字段 | 含义     | 数据类型 | 长度 | 是否允许为空 | 备注 |
| ---- | -------- | -------- | ---- | ------------ | ---- |
| CID  | 课程号   | varChar  | 255  | 否           | 主键 |
| Name | 课程名称 | varChar  | 255  | 否           |      |
| TID  | 老师工号 | varChar  | 255  | 否           |      |

④Hubu_score表

| 字段  | 含义     | 数据类型 | 长度 | 是否允许为空 | 备注       |
| ----- | -------- | -------- | ---- | ------------ | ---------- |
| ID    | id       | int      |      | 否           | 主键       |
| CID   | 课程号   | varChar  | 255  | 否           | 外键当主键 |
| SID   | 学生学号 | varChar  | 255  | 否           | 外键当主键 |
| Score | 成绩     | varChar  | 255  | 否           |            |



##### 功能设计

学生信息管理模块包括增加、删除、修改、查询等。

（1） 当有新生入学或有学生转入时，需要将其基本信息，如姓名、学号、所在院校、专业班级等录入到学生档案中。 

（2） 当学生毕业或有学生退学时，学需将其基本信息从学生档案中删除。 

（3） 当有学生转专业时，需要将其基本信息进行修改。 

（4） 当需要得到某个学生相关信息时就要进行查询。 

 

成绩信息管理模块：

该模块主要是实现对学生成绩的录入查询工作，包括：

（1）学生成绩的录入

（2）学生成绩查询